package ru.kondratenko.jse56.process;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.kondratenko.jse56.model.Chat;
import ru.kondratenko.jse56.model.Message;
import ru.kondratenko.jse56.model.User;
import ru.kondratenko.jse56.rabbitmq.MessageReceiver;
import ru.kondratenko.jse56.repository.ChatRepository;
import ru.kondratenko.jse56.repository.MessageRepository;
import ru.kondratenko.jse56.repository.UserRepository;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.util.*;

@Log4j2
@Component
public class ApplicationProcess implements CommandLineRunner {

    private static UserRepository userRepository;
    private static ChatRepository chatRepository;
    private static MessageRepository messageRepository;
    private final CachingConnectionFactory connectionFactory;
    public final Scanner scanner;
    private final RabbitTemplate template;

    private FanoutExchange exchange;

    private SimpleMessageListenerContainer container;

    private static String USER_TEXT = "Пользователь";
    private static String CHAT_TEXT = "Чат";
    private static String EXIT = "exit";

    private User user;
    private Chat chat;


    @Autowired
    public ApplicationProcess(UserRepository userRepository,
                              ChatRepository chatRepository,
                              MessageRepository messageRepository,
                              RabbitTemplate template,
                              CachingConnectionFactory connectionFactory
    ) {
        this.userRepository = userRepository;
        this.chatRepository = chatRepository;
        this.messageRepository = messageRepository;
        this.scanner = new Scanner(System.in);
        this.template = template;
        this.connectionFactory = connectionFactory;
    }

    private User login(){
        System.out.println(USER_TEXT + ":");
        String nameUser = scanner.nextLine();
        Optional<User> userOptional = userRepository.findByName(nameUser);
        if(userOptional.isEmpty()) {
            return userRepository.save(new User(nameUser));
        }
        else{
            return userOptional.get();
        }
    }

    private Chat findChat(){
        System.out.println(CHAT_TEXT + ":");
        String nameChat = scanner.nextLine();
        Optional<Chat> chatOptional = chatRepository.findByName(nameChat);
        if(chatOptional.isEmpty()) {
            return chatRepository.save(new Chat(nameChat));
        }
        else{
            return chatOptional.get();
        }
    }


    @Override
    public void run(String... args) throws Exception {
        String command = "";
        user = login();
        chat = findChat();
        setupRabbit(chat.getName());
        while (scanner.hasNextLine()) {
            command = scanner.nextLine();
            if (command.toLowerCase(Locale.ROOT).equals(EXIT)) break;
            Message message = new Message();
            message.setMessage(command);
            message.setTime(Instant.now());
            message.setUserId(user.getId());
            message.setChatId(chat.getId());
            messageRepository.save(message);
            template.convertAndSend(exchange.getName(), "", user.getName() + ": " + message.getMessage());
        }
        container.stop();
        connectionFactory.resetConnection();
    }

    private void ensureExchangeExist(Channel channel, String exchangeName) throws IOException {
        try {
            channel.exchangeDeclarePassive(exchangeName);
        } catch (Exception e) {
            channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT);
        }
    }

    private void buildListener(String queueName) {
        MessageListenerAdapter adapter = new MessageListenerAdapter(new MessageReceiver(), "receiveMessage");
        container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(adapter);
        container.initialize();
        container.start();
    }

    private void setupRabbit(String chatName) {
        exchange = new FanoutExchange(chatName);
        try (Connection connection = connectionFactory.createConnection();
             Channel channel = connection.createChannel(false)
        ) {
            ensureExchangeExist(channel, exchange.getName());
            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, exchange.getName(), "");
            buildListener(queueName);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}

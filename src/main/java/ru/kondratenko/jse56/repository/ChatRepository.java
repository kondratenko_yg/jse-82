package ru.kondratenko.jse56.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kondratenko.jse56.model.Chat;


import javax.transaction.Transactional;
import java.util.Optional;
@Transactional
public interface ChatRepository extends JpaRepository<Chat, Long> {

    Optional<Chat> findByName(String name);

}

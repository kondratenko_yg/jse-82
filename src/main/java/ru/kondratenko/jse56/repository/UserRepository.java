package ru.kondratenko.jse56.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kondratenko.jse56.model.User;


import javax.transaction.Transactional;
import java.util.Optional;
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByName(String name);

}

package ru.kondratenko.jse56.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kondratenko.jse56.model.Message;

import javax.transaction.Transactional;

@Transactional
public interface MessageRepository extends JpaRepository<Message, Long> {


}
